#Project Morgemil

Project Morgemil is the game engine which will provide the basis for Daniel's game world.

Morgemil's design philosophy is based in [data-oriented design](http://www.dataorienteddesign.com/dodmain/) and specialized towards the concept of an [entity-component-system](http://en.wikipedia.org/wiki/Entity_component_system). 

* An "Entity" is an abstract concept of something that exists. An Entity has no data or behavior whatsoever; in fact, it only exists as a unique number designation. 
* A "Component" is only data. For example, there exists a "Position" Component that is (X, Y, Z) coordinates. 
* A "System" is behavior/logic. For example, the "Physics" System operates upon and updates the "Position" Component at set time intervals.

Because of this: every conceptual item is broken into parts, let us take a generic NPC for example. 

 * NPC Components 
     + Position (The coordinates of this guy in the world)
     + Collision (The physics info)
     + Render (The visual representation of this NPC)
     + Dialogue (The player has to talk to the NPC of course)
  * Relevant Systems (The Components the System is interested in are in the carat signs)
     + ChatSystem <Dialogue,Position>   (A player can "talk" to this entity if the entity has a Dialogue and a Position. Presumably the position has to be within a certain radius of the player)
     + RenderSystem <Render,Position>  (The NPC has to be drawn to the screen somehow, but of course only if the position is on screen)
     + PhysicsSystem <Collision,Position>   (Movement, Spells, whatever interacts with the physics can cause the entity's position to change)

Notice something interesting? The "NPC" is not declared explicitly as a NPC. It is merely an entity with some extra data. The player could theoretically talk to a tree if that tree has a Dialogue component. Since each System, set of logic, is only interested in a potential subset of data, an Entity may gain new sets of behavior simply by adding a new Component.

Without taking the time to fully explain the logic behind my interpretation of data-oriented-design, let me just list some benefits I'm gaining here:
 
* Cache-efficiency. Computer memory may be random-access but abusing that is simply not as efficient as storing everything sequentially. This allows a continuous "stream" of data from RAM to the CPU which makes the intermediary caches very happy.
* Memory pools. This is the method by which cache-efficiency is increased. This also eliminates most memory allocations. A lack of memory allocations means less interaction with the operating system's kernel which has numerous beneficial effects decreasing overhead costs.
* Isolated functionality. From a software developer's viewpoint (mine at least), maintenance is the primary concern of any long-term project. By isolating Systems to subsets of data, they directly affect less things which is good design.
* Parallelism. Isolation has added benefits. Each set of components is divided by entity. IE: CPU 1 can work on Entity4's Components without risk of hurting Entity5's Components (which could be worked on in parallel in some other CPU). A different way of looking at parallelism is by System. A System that only works on the Dialogue Component will not interfere with a System that only works on the Position Component. (IE: The data for those two logical systems does not overlap)
* Extensibility. Again, maintenance. Somewhere down the road, new functionality will be desired. When this happens, it will be as easy as creating new Components to hold data and/or creating new Systems to manipulate data. This eliminates extending/modifying monolithic class inheritance.
* Modding. By keeping logic and data separate, pretty much all obstacles to modding disappear. Hand-in-hand is the ability to add scripting.
* Networking. By breaking data into discrete portions, much redundant network traffic can be eliminated.
* Storage. Discrete chunks of data is much easier to edit and store. In fact, data-oriented-design is merely an extension of a [Relational-Database](http://en.wikipedia.org/wiki/Relational_database_management_system): instant compatibility.

##Morgemil Technology
The entire engine is currently written in C++ (the C++11 standard).

To say I'm abusing the compiler is an understatement. Most of the engine core is header only templates which guarantees slow compile time. To make matters worse: recursive variadic templates and type inference are rampant. Thankfully, the cost is mostly compile time. Through the use of aggressive default templates, virtual functions are minimal.

The performance gains over the previous language (C#.NET) is absolutely astoundingly incomparable. ["Premature optimization is the root of all evil"](http://en.wikipedia.org/wiki/Program_optimization#When_to_optimize) but it is very necessary in the case of a game engine. Also, by switching to C++, native support for Linux/Mac is gained.


 [Markdown reference](https://bitbucket.org/tutorials/markdowndemo) (the syntax to format this stuff)