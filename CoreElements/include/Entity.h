#ifndef ENTITY_H
#define ENTITY_H

#include <iostream>

#include "EntityUID.h"
#include "EntityBase.h"
#include "BaseComponentType.h"

namespace Morgemil
{
  ///Forward declaration
  template<typename...Us>
  class ComponentWorld ;

  ///########################################################################
  /** A sequence of Components. */
  template<class...Ts>
  class Entity:
    EntityBase< typename BaseComponentType<Ts>::ComponentType >...
  {
  public:
    /** The world this entity is a part of */
    typedef ComponentWorld< Ts... > EntityWorld;
    typedef EntityWorld* EntityWorldPtr;
  private:
    /// The Entity's Unique ID
    const UniqueID m_ID = 0;
    /// The Entity's containing world
    const EntityWorldPtr m_World = nullptr;
  protected:
  public:
    ///########################################################################
    Entity(UniqueID id,
           EntityWorldPtr world ):
      m_ID(id),
      m_World(world)
    {
      //std::cout << "Generated id - " << m_ID << std::endl;
    }
    ///########################################################################
    void Release()
    {
      m_World->Release( this );
    }
    ///########################################################################
    ~Entity() {}
    ///########################################################################
    ///The Unique ID
    UniqueID UID() const { return m_ID; }
    ///########################################################################
    ///The Entity's World
    EntityWorldPtr World() const { return m_World; }
    ///########################################################################
    /// Mutators
    template<class TItem >
    void Set( TItem* arg )
    {
      this->EntityBase<TItem>::Set( arg );
    }
    ///########################################################################
    /// Accessors Base
    template<class TItem >
    void Get( TItem* &arg )
    {
      this->EntityBase<TItem>::Item( arg );
    }
    ///########################################################################
    /// Accessors Recursive
    template<class TItem,
             class ... TList >
    void Get(TItem* &arg,
             TList && ... args )
    {
      this->Get( arg );
      this->Get( std::forward<TList>( args )... );
    }
    ///########################################################################
    /// World Wrapper
    template<class TItem = Component,
             class...Fwd>
    TItem* Attach(Fwd && ... args )
    {
      return m_World->Attach<TItem>(this,
                                    args... );
    }
    ///########################################################################
    /// World Wrapper
    template<class TItem = Component>
    void Detach()
    {
      this->EntityBase<TItem>::Set( nullptr );
      m_World->Detach<TItem>(this );
    }
    ///########################################################################
  };
  ///########################################################################

}
#endif // ENTITY_H
