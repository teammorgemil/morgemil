#ifndef ENTITY_BASE_H
#define ENTITY_BASE_H

#include "Component.h"
#include "ComponentCollection.h"

namespace Morgemil
{
  ///########################################################################
  /** A single Component. */
  template<class T = Component >
  class EntityBase
  {
  private:
    T *m_Component = nullptr;
  protected:
  public:
    ///########################################################################
    EntityBase() {}
    ~EntityBase() {}
    ///########################################################################
    /** The contained component */
    void Item(T* &arg)
    {
      arg = m_Component;
    }
    ///########################################################################
    /** The contained component */
    void Set(T* arg)
    {
      m_Component = arg;
    }
    ///########################################################################
  };
  ///########################################################################

}
#endif // ENTITY_BASE_H
