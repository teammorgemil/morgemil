#ifndef BASE_COMPONENT_TYPE_H
#define BASE_COMPONENT_TYPE_H
namespace Morgemil
{
  #include "ComponentCollection.h"
  /**
   *  Associates the type with the ComponentCollection
   */
  template<class T = ComponentCollection<Component> >
  struct BaseComponentType
  {
    ///So the Entity may resolve issues
    typedef typename T::ComponentType ComponentType;

  };

}
#endif // BASE_COMPONENT_TYPE_H
