#ifndef MATCH_COMPONENT_WITH_COLLECTION
#define MATCH_COMPONENT_WITH_COLLECTION

#include "BaseComponentType.h"

namespace Morgemil
{


  template<class T, class T2, class...args >
  struct MatchComponentWithCollection
  {
    typedef typename std::conditional<std::is_same<T, typename BaseComponentType<T2>::ComponentType >::value,
                                      T2,
                                      typename MatchComponentWithCollection<T, args... >::type >::type type;
  };


  template<class T, class T2 >
  struct MatchComponentWithCollection<T, T2 >
  {
    typedef T2 type;
  };

}
#endif // MATCH_COMPONENT_WITH_COLLECTION
