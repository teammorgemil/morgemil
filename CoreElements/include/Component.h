#ifndef COMPONENT_H
#define COMPONENT_H

#include "EntityUID.h"
#include <iostream>

namespace Morgemil
{
  ///########################################################################
  /**
   *This is a mechanism for determining if a component is active.
   *Also, each component should have a default blank constructor that is the
   *value until the user replaces with actual data and the Active flag is toggled
   */
  class Component
  {
  private:
    bool m_Active = false;
  protected:
  public:
    /** Default constructor */
    Component() {}
    /** Default destructor */
    ~Component() {}

    bool IsActive() { return m_Active; }
    void SetActive(bool active) { m_Active = active; }

    void Release() { SetActive(false); }
    void Activate() { SetActive(true); }
  };
  ///########################################################################

}
#endif // COMPONENT_H
