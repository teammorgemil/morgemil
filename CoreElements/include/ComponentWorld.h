#ifndef COMPONENTWORLD_H
#define COMPONENTWORLD_H

#include <unordered_map>

#include "Entity.h"
#include "Component.h"
#include "ComponentWorldBase.h"
#include "EntityUIDCollection.h"
#include "MatchComponentWithCollection.h"

namespace Morgemil
{
  ///########################################################################
  /** A sequence of entities with the given Components. */
  template<class...Ts>
  class ComponentWorld: ComponentWorldBase<Ts >...
  {
  public:
    ///typedefs
    /** Shorthand for this world's entity */
    typedef Entity< Ts... > WorldEntity;
    typedef WorldEntity* WorldEntityPtr;
    /** Matches component with collection */
    template< class T >
    using MatchedCollection = typename MatchComponentWithCollection< T, Ts...>::type;

  private:
    EntityUIDCollection m_UIDCollection;
    std::unordered_map<UniqueID, WorldEntity> m_EntityCollection;
  protected:
  public:
    ComponentWorld() {}
    ~ComponentWorld() {}
    /** Returns a newly generated entity pointer to fill with active components */
    WorldEntityPtr Generate();
    /** Releases an entity into the wild, to be used again somewhere. Someday. */
    void Release(WorldEntityPtr arg );
    ///########################################################################
    /**
     * Returns a created Component
     */
    template<class C = Component,
             class...Fwd>
    C* Attach(WorldEntityPtr ent,
              Fwd && ... args )
    {
      C* RetVal = nullptr;
      this->ComponentWorldBase< MatchedCollection<C> >::Attach(RetVal,
                                                               ent->UID(),
                                                               args... );
      ent->Set( RetVal );
      return RetVal;
    }
    ///########################################################################
    /**
     * Removes a Component
     */
    template<class C = Component>
    void Detach(WorldEntityPtr ent )
    {
      ///Used for type inference
      C* nothing = nullptr;
      ent->Set( nothing);
      this->ComponentWorldBase< MatchedCollection<C> >::Detach( ent->UID() );
    }
    ///########################################################################
  };
  ///########################################################################
  /** Returns a newly generated entity pointer to fill with active components */
  template<class...Ts>
  typename ComponentWorld<Ts...>::WorldEntityPtr ComponentWorld<Ts...>::Generate()
  {
    UniqueID nextID = m_UIDCollection.Reserve();
    auto result = m_EntityCollection.emplace(std::piecewise_construct,
                                             std::forward_as_tuple(nextID),
                                             std::forward_as_tuple(nextID,
                                                                   this) );
    return &(result.first->second);
  }
  ///########################################################################
  /** Releases an entity into the wild, to be used again somewhere. Someday. */
  template<class...Ts>
  void ComponentWorld<Ts...>::Release( WorldEntityPtr arg )
  {
    UniqueID id = arg->UID();
    m_EntityCollection.erase( id );
    m_UIDCollection.Release( id );
    //std::cout << "Released id - " << id << "\r\n";
  }
  ///########################################################################
}
#endif // COMPONENTWORLD_H
