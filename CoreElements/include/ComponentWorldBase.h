#ifndef COMPONENTWORLDBASE_H
#define COMPONENTWORLDBASE_H

#include "ComponentCollection.h"

namespace Morgemil
{
  ///########################################################################
  /**
   * This wrappers the collection
   */
  template<class CollT = ComponentCollection<Component> >
  class ComponentWorldBase
  {
  public:
    /** The collection this wrappers */
    typedef CollT CollectionType;

    typedef typename BaseComponentType<CollectionType>::ComponentType ComponentType;

  private:
    CollT m_wrapped;
  protected:
  public:
    ComponentWorldBase() {}
    ~ComponentWorldBase() {}
    ///########################################################################
    template<class...Fwd>
    void Attach(ComponentType* & RetVal,
                UniqueID Id,
                Fwd && ... args )
    {
      RetVal = m_wrapped.Attach(Id,
                                args... );
    }
    ///########################################################################
    void Detach( UniqueID Id )
    {
      m_wrapped.Detach(Id );
    }
    ///########################################################################
    void two() {};
  };
  ///########################################################################
}
#endif // COMPONENTWORLDBASE_H
