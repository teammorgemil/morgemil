#ifndef MORGEMIL_BODY_H
#define MORGEMIL_BODY_H

#include "Circle.h"
#include "Component.h"
namespace Morgemil
{
  ///########################################################################
  class CompBody : public Component
  {
  private:
    Circle m_area;
  protected:
  public:
    /**Constructors*/
    CompBody() {}
    /**Accessor*/
    inline Circle& Area() { return m_area; }
  };
  ///########################################################################
}
#endif // MORGEMIL_BODY_H
