#ifndef ENTITY_UID_COLLECTION_H
#define ENTITY_UID_COLLECTION_H

#include <set>

#include "EntityUID.h"

namespace Morgemil
{
  ///########################################################################
  /** A sequence of Unique IDs */
  class EntityUIDCollection
  {
  private:
    /** Choose these next before incrementing the Sequence number */
    std::set<UniqueID> m_Open;
    /** The currently live ids */
    std::set<UniqueID> m_Current;
    /**The next number from which to choose a new id */
    UniqueID m_SequenceTop = 1;
  protected:
  public:
    EntityUIDCollection() {}
    ~EntityUIDCollection() {}

    /** Remove a unique id from the current set */
    void Release(UniqueID id);
    /** Reserve a new unique id */
    UniqueID Reserve();
    /** Reserve a specified unique id. Returns success */
    bool Reserve(UniqueID id);
    /** This ID currently exists */
    bool Alive(UniqueID id);
  };
  ///########################################################################
  /** Remove a unique id from the current set */
  void EntityUIDCollection::Release(UniqueID id)
  {
    auto it = m_Current.find(id);
    if( it != m_Current.end() )
    {
      m_Current.erase( it );
    }
    if( m_Open.find(id) == m_Open.end() )
    {
      m_Open.insert(id);
    }
  }
  ///########################################################################
  /** Reserve a new unique id */
  UniqueID EntityUIDCollection::Reserve()
  {
    UniqueID result = 0;
    if( m_Open.size() > 0 )
    {
      result = *( m_Open.begin() );
      m_Open.erase( m_Open.begin() );
    }
    else
    {
      while( !Reserve(m_SequenceTop) );
      result = m_SequenceTop - 1;
    }
    return result;
  }
  ///########################################################################
  /** Reserve a specified unique id. Returns success */
  bool EntityUIDCollection::Reserve(UniqueID id)
  {
    if( m_Current.find(id) == m_Current.end() )
    {
      m_Current.insert(id);
      if(m_SequenceTop < id)
      {
        if(m_Open.find(id) != m_Open.end())
        {
          m_Open.erase(id);
        }
      }
      else
      {
        m_SequenceTop = std::max(m_SequenceTop, id + 1);
      }
      return true;
    }
    return false;
  }
  ///########################################################################
  /** This ID currently exists */
  bool EntityUIDCollection::Alive(UniqueID id)
  {
    return m_Current.find(id) != m_Current.end();
  }
  ///########################################################################
}
#endif // ENTITY_UID_COLLECTION_H

