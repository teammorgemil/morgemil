#ifndef MORGEMIL_CIRCLE_H
#define MORGEMIL_CIRCLE_H

#include "Vector2.h"
namespace Morgemil
{
  class Circle
  {
  private:
    Vector2f m_center = Vector2f(0.0,0.0);
    double m_radius = 0;
  protected:
  public:
    /**Constructors*/
    Circle() {};
    Circle(Vector2f center,
           double radius)
    {
      m_center = center;
      m_radius = radius;
    }
    /**Accessor*/
    inline double& Radius() { return m_radius; }
    inline Vector2f& Center() { return m_center; }
    /**Container*/
    inline bool Contains(Vector2f& location)
    {
      return (location.Subtract(m_center).LengthSquared() <= m_radius*m_radius);
    }
    /**Collision*/
    inline bool Collides(Circle& circ)
    {
      return (circ.Center().Subtract(m_center).LengthSquared() <= (m_radius+circ.Radius()) * (m_radius+circ.Radius()));
    }

  };
}
#endif // MORGEMIL_CIRCLE_H
