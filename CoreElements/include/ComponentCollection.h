#ifndef COMPONENT_COLLECTION_H
#define COMPONENT_COLLECTION_H

#include <map>

#include "Component.h"
#include "EntityUID.h"

namespace Morgemil
{
  ///########################################################################
  /**
   * A collection to abstract the allocation process of handling a series
   *   of pointers to fixed memory components.
   */
  template<class TComp = Component>
  class ComponentCollection
  {
  public:
    ///The ComponentWorld uses this
    typedef TComp ComponentType;
    typedef TComp* ComponentTypePtr;
  private:
    std::map<UniqueID, ComponentType> m_Items;
  protected:
  public:
    ComponentCollection() {}
    ~ComponentCollection() {}
    ///########################################################################
    template<class...Fwd>
    ComponentTypePtr Attach(UniqueID id,
                            Fwd && ... args )
    {
      auto result = m_Items.emplace(std::piecewise_construct,
                                    std::forward_as_tuple(id),
                                    std::forward_as_tuple(args...) );
      return &(result.first->second);
    }
    ///########################################################################
    template<class...Fwd>
    void Detach(UniqueID id )
    {
      m_Items.erase(id);
    }
    ///########################################################################
  };
  ///########################################################################
}
#endif //COMPONENT_COLLECTION_H
