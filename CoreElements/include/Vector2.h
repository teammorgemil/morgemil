#ifndef MORGEMIL_VECTOR2_H
#define MORGEMIL_VECTOR2_H

#include <math.h>
namespace Morgemil
{
  template<typename T = double>
  class Vector2
  {
  private:
    T m_X = 0;
    T m_Y = 0;

  public:
    Vector2(T x = 0,
            T y = 0) :
      m_X(x),
      m_Y(y)
    {
    }
    ///##########################################################################
    /**Accessor X*/
    inline T& X() { return m_X; }
    /**Accessor Y*/
    inline T& Y() { return m_Y; }
    /** Length Squared. More efficient.*/
    inline T LengthSquared() const
    {
      return (m_X*m_X) + (m_Y*m_Y);
    }
    /** sqrt of LengthSquared.*/
    inline T Length() const
    {
      return sqrt(LengthSquared());
    }
    ///##########################################################################
    /**Absolute values*/
    inline Vector2<T> Absolute()
    {
      return Vector2<T>(abs(m_X),
                        abs(m_Y));
    }
    /**Normalize to a length of 1*/
    inline Vector2<T> Normalize()
    {
      T len = Length();
      Vector2<T> result;
      if( len < -0.00001 || len > 0.000001)
      {
        return result.Divide(len);
      } else {
        return result;
      }
    }
    ///##########################################################################
    /**Additions*/
    inline Vector2<T> Add(Vector2<T> &vec )
    {
      return Add(vec.X(),
                 vec.Y());
    }
    inline Vector2<T> Add(T x,
                           T y)
    {
      Vector2<T> result(m_X + x,
                        m_Y + y);
      return result;
    }
    inline Vector2<T> Add(T len)
    {
      return Add(len,
                 len);
    }
    ///##########################################################################
    /**Subtractions*/
    inline Vector2<T> Subtract(Vector2<T> &vec )
    {
      return Subtract(vec.X(),
                      vec.Y());
    }
    inline Vector2<T> Subtract(T x,
                                T y)
    {
      return Add(-x,
                 -y);
    }
    inline Vector2<T> Subtract(T len)
    {
      return Subtract(len,
                      len);
    }
    ///##########################################################################
    /**Multiply*/
    inline Vector2<T> Multiply(Vector2<T> &vec )
    {
      return Multiply(vec.X(),
                      vec.Y());
    }
    inline Vector2<T> Multiply(T x,
                                T y)
    {
      Vector2<T> result(x * m_X,
                        y * m_Y);
      return result;
    }
    inline Vector2<T>& Multiply(T len)
    {
      return Multiply(len,
                      len);
    }
    ///##########################################################################
    /**Divide*/
    inline Vector2<T> Divide(Vector2<T> &vec )
    {
      return Divide(vec.X(),
                    vec.Y());
    }
    inline Vector2<T> Divide(T x,
                              T y)
    {
      Vector2<T> result(m_X/x,
                        m_Y/y);
      return result;
    }
    inline Vector2<T> Divide(T len)
    {
      return Divide(len,
                    len);
    }
    ///##########################################################################

  };


  /**int*/
  typedef Vector2<int> Vector2i;
  /**double*/
  typedef Vector2<double> Vector2f;


}
#endif // MORGEMIL_VECTOR2_H


