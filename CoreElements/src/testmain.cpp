#include <iostream>
#include <chrono>
#include <ctime>

#include "Entity.h"
#include "CompBody.h"

#include "ComponentWorld.h"

class Comp33 : public Morgemil::Component
{
public:
    Comp33()
    {
      //std::cout << "No-Constructor" << std::endl;
    }
    Comp33(int i )
    {
      //std::cout << "Constructor-" << i << std::endl;
    }

    ~Comp33()
    {
      //std::cout << "Destructor" << std::endl;
    }

};



typedef Morgemil::ComponentCollection< Comp33 > SomeColl33;
typedef Morgemil::ComponentCollection< Morgemil::CompBody > SomeCollection;



int main( int argc, char* argv[] )
{

  std::chrono::time_point<std::chrono::system_clock> start, endtime;
  start = std::chrono::system_clock::now();


/*
 *   Morgemil::CompBody *itemasdfad = nullptr;
 *   Comp33 *c33 = nullptr;
 *
 *   Morgemil::ComponentWorld< Morgemil::CompBody, Comp33 > someWorld;
 *   Morgemil::ComponentWorld< Morgemil::CompBody, Comp33 >::WorldEntityPtr someEntity = someWorld.Generate();
 *
 *   someEntity->Get( c33, itemasdfad );
 *
 *   someEntity->Release();
 *
 *   someWorld.Release( someWorld.Generate() );
 *
 *   someWorld.Attach< Morgemil::CompBody >(someEntity);
 *
 */
  Morgemil::ComponentWorld< SomeColl33, SomeCollection > theWorld;

  Comp33 * c33 = nullptr;
  Morgemil::CompBody *itemasdfad = nullptr;

  for(int i = 0; i < 1000 * 1000; i++ )
  {
    auto theEntity = theWorld.Generate();
    theEntity->Get( c33, itemasdfad );
    //std::cout << (c33 == nullptr ? "Empty": "Active") << std::endl;

    c33 = theEntity->Attach<Comp33>();
    //std::cout << (c33 == nullptr ? "Empty": "Active") << std::endl;

    theWorld.Detach<Comp33>(theEntity);

    theEntity->Get( c33, itemasdfad );
    //std::cout << (c33 == nullptr ? "Empty": "Active") << std::endl;
    theWorld.Release( theEntity );
  }



  endtime = std::chrono::system_clock::now();

  std::chrono::duration<double> elapsed_seconds = endtime-start;
  std::time_t end_time = std::chrono::system_clock::to_time_t(endtime);

  std::cout << "\r\nfinished computation at " << std::ctime(&end_time)
            << "elapsed time: " << elapsed_seconds.count() << "s\n";


  int wait;
  std::cin >> wait;

  return 0;
}

